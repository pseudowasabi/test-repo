
package first_folder;

import first_folder.second_folder_revision.SubTestClass01Impl_Temp.TestMethod;

public class TestClass01 {

    private static final ONE = 1;
    private static final TWO = 2;

    public static void main(String[] args) {
        System.out.println("Bitbucket test");

        Object returnValue = TestMethod("a-e-i-o-u", "bitbucket vs github");

        System.out.println(returnValue.toString());
        System.out.println("test complete");
    }

}